
  function xhrGetIntro() {
    return $.ajax({
      type: 'POST',
      url: '../mobile/pages/intro.php',
      success: function(data) {
        $('#intro').html(data);
      }
    });
  }

  function xhrGetAbout() {
    return $.ajax({
      type: 'POST',
      url: '../mobile/pages/about.php',
      success: function(data) {
        $('#about').html(data);
      }
    });
  }

  function xhrGetSoftware() {
    return $.ajax({
      type: 'POST',
      url: '../mobile/pages/software.php',
      success: function(data) {
        $('#selection').html(data);
      }
    });
  }

  function xhrGetSupport() {
    return $.ajax({
      type: 'POST',
      url: '../mobile/pages/support.php',
      success: function(data) {
        $('#selection').html(data);
      }
    });
  }


  function xhrGetSelection() {
    return $.ajax({
      type: 'POST',
      url: '../mobile/pages/selection.php',
      success: function(data) {
        $('#selection').html(data);
      }
    });
  }
  
  function xhrGetContact() {
    return $.ajax({
      type: 'POST',
      url: '../mobile/pages/contact.php',
      success: function(data) {
        $('#contact').html(data);
      }
    });
  }

  function xhrSubmitForm() {
    return $.ajax({
      type: 'POST',
      data: $('#contact_form').serialize(),
      url: '../contact/submit_form.php',
      success: function(data) {
        if (data == "Mail sent") {
          alert("Thank you for contacting me, the form was sent succesfully. \nI will get back to you as soon as possible.");
        } else {
          alert("There was an issue sending the contact form, please try again later.");          
        }
      }
    });
  }

$(function() {
  var menuVisible = false;
  $('#nav_hamburger').click(function() {
    if (menuVisible) {
      $('#nav_links').css({'display':'none'});
      menuVisible = false;
      return;
    }
    $('#nav_links').css({'display':'block'});
    menuVisible = true;
  });
  $('.nav_buttons').click(function() {
    $('#nav_links').css({'display':'none'});
    menuVisible = false;
  });
});

function isScrolledIntoView(elem)
{
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}


$(document).ready(function() { 

  xhrGetIntro().done(function(){});
  xhrGetAbout().done(function(){});
  xhrGetSelection().done(function(){});
  xhrGetContact().done(function(){});
  
  var lastScrollTop = 0;

  function onScroll() {
    var st = $(this).scrollTop();
    var elemTop = $("#intro").offset().top;
    var introBottom = elemTop + $("#intro").height();
    if (st > lastScrollTop){
      $("#up_arrow").css("display", "none");
    } else if (st < lastScrollTop && st > introBottom) {
      $("#up_arrow").css("display", "block");
    }
    lastScrollTop = st;
  }

  $(document.body).on('touchmove', onScroll); // for mobile
  $(window).on('scroll', onScroll); 

  $( ".nav_buttons" ).each(function() {
    $(this).on("click", function(){
        var pageName = $(this).attr('name');
        if (pageName == "skills") {
          $("#select_content").css("display", "none");
          $("#selection").css("display", "flex");
          pageName = "selection";
          var scrollTop = $("body").scrollTop() + $("#select_start").offset().top;      
          $('html, body').animate({
              scrollTop: scrollTop
          }, 500);  
          window.location = '#'+pageName;
        } else if (pageName == "about") {
          var scrollTop = $("body").scrollTop() + $("#about_start").offset().top;      
          $('html, body').animate({
              scrollTop: scrollTop
          }, 500);  
          window.location = '#'+pageName;
        } else if (pageName == "contact") {
          var scrollTop = $("body").scrollTop() + $("#contact_start").offset().top;      
          $('html, body').animate({
              scrollTop: scrollTop
          }, 500);  
          window.location = '#'+pageName;
        }
    });
  });

  $(document.body).on('click',"#up_arrow",function (event) {
    var scrollTop = $("body").scrollTop() + $(".topnav").offset().top;      
    $('html, body').animate({
        scrollTop: scrollTop
    }, 500, function() {
      window.location = '#intro';
      $("#up_arrow").css("display", "none");
    });  
  });

  $(document.body).on('click',"#send_button",function (event) { 
    $('#send_button').prop("disabled", true);
    xhrSubmitForm().done(function(){});
  });

  $(document.body).on('click',"#programming_select",function (event) { 
    xhrGetSoftware().done(function(){});
    $("body").animate({ scrollTop: $("#software").offset().top }, 1000);
  });

  $(document.body).on('click',"#support_select",function (event) { 
    xhrGetSupport().done(function(){});
    $("body").animate({ scrollTop: $("#support").offset().top }, 1000);
  });

  $(document.body).on('click',"#return_icon",function (event) { 
    xhrGetSelection().done(function(){});
  });
  
  $(document.body).on('click',"#create_db_button",function (event) {
    event.preventDefault();
    console.log("clicked");
    $db_name = $("#create_db_name").val();
    xhrCreateDB($db_name).done(function() {
      xhrShowEmployeeTable($db_name).done(function() {});
    });
    $(".create_db_form").css("display", "none");
  });

  $(document.body).on('click',"#create_employee_button",function (event) {
    event.preventDefault();
    $db_name = $(this).attr("name");  
    xhrAddEmployee($db_name).done(function() {
      xhrShowEmployeeTable($db_name).done(function() {});});
  });

  $(document.body).on('click',".delete_employee",function (event) {
      event.preventDefault();
      $db_name = $(this).attr("name");  
      $id = $(this).attr("id");  
      console.log($id);
      xhrDeleteEmployee($db_name, $id).done(function() {
        xhrShowEmployeeTable($db_name).done(function() {});});
  });

  $(document.body).on('click',".edit_employee",function (event) {
    event.preventDefault();
    $(".td_save_edit_employee").css("display", "inline-block");
    $(".td_edit_employee").css("display", "none");
    $('input[name ="edit_first_name"]').prop("readonly", false);
    $('input[name ="edit_last_name"]').prop("readonly", false);
    $('input[name ="edit_employee_age"]').prop("readonly", false);
    $('input[name ="edit_employee_salary"]').prop("readonly", false);
    $('input[name ="edit_employee_role"]').prop("readonly", false);
    $('.edit_employee_input').removeClass("input_noneditable");
  });

  $(document.body).on('click',".save_edit_employee",function (event) {
    event.preventDefault();
    $db_name = $(this).attr("name");  
    $id = $(this).attr("id");  
    $(".td_save_edit_employee").css("display", "none");
    $(".td_edit_employee").css("display", "inline-block");
    xhrEditEmployee($db_name, $id).done(function() {
      xhrShowEmployeeTable($db_name).done(function() {});});
      $('.edit_employee_input').addClass("input_noneditable");
      $('input[name ="edit_first_name"]').prop("readonly", true);
      $('input[name ="edit_last_name"]').prop("readonly", true);
      $('input[name ="edit_employee_age"]').prop("readonly", true);
      $('input[name ="edit_employee_salary"]').prop("readonly", true);
      $('input[name ="edit_employee_role"]').prop("readonly", true);
  });

});
