<?php
if (session_status() == PHP_SESSION_NONE) {
  session_start();
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="css/index.css">
        <title>Gilles Boonen</title>
        <script type="text/javascript">
            if (screen.width >= 720) {
            window.location = "../index.php";
        }
        </script>
    </head>
    <body>
        <!-- Top Navigation Menu -->
        <!-- Top Navigation Menu -->
        <div class="topnav">
          <a href="#home">Gilles Boonen</a>
          <!-- Navigation links (hidden by default) -->
          <div id="nav_links">
              <a id="about_nav" class="nav_buttons" name="about" >about</a>
              <a id="skills_nav" class="nav_buttons" name="skills">skills</a>
              <a id="contact_nav" class="nav_buttons" name="contact">contact</a>
          </div>
          <!-- "Hamburger menu" / "Bar icon" to toggle the navigation links -->
          <a href="javascript:void(0);" id="nav_hamburger" class="icon">
            <i style="color:rgb(255, 255, 255);" class="fa fa-bars"></i>
          </a>
        </div>
        <div id="root">
          <img id="up_arrow" src="img/up_arrow.png" alt="back to top">
          <div id="intro">
          </div>    
          <div id="about_start" class="section_divider"></div>
          <div id="about">
          </div>    
          <div id="select_start" class="section_divider"></div>
          <div id="selection">
          </div>  
          <div id="contact_start" class="section_divider"></div>
          <div id="contact">
          </div>  
        </div>           
        <script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous"></script>
        <script src="js/index.js"></script>
      </body>
</html>