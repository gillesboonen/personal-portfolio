<?php
// include $_SERVER['DOCUMENT_ROOT'].'/classes/aboutview.class.php';
?>
<div class="page_content_select">
    <div id="programming_select">
        <div>Software Development</div>
        <span class="photo_credit">Photo by <a target="_blank" href="https://unsplash.com/@codestorm?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Safar Safarov</a> on <a target="_blank" href="https://unsplash.com/s/photos/information-technology?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Unsplash</a></span>
    </div>
    <div class="section_divider"></div>
    <div id="support_select">
        <div>Technical Support</div>    
        <span class="photo_credit">Photo by <a target="_blank" href="https://unsplash.com/@marvelous?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Marvin Meyer</a> on <a target="_blank" href="https://unsplash.com/s/photos/information-technology?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Unsplash</a></span>
    </div>
</div>
