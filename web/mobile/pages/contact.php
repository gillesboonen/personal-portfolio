<div class="page_content">
    <div class="contact_header">
        <h2>Get in touch</h2>
    </div>
    <div class="contact_body">
      <form id="contact_form" action="POST">
        <input class="w3-input contact_input" type="text" name="name" placeholder="Name">
        <input class="w3-input contact_input" type="email" name="email" placeholder="E-mail">
        <textarea class="w3-textarea contact_input" name="text_body" id="contact_text_body" cols="30" rows="5" placeholder="Text"></textarea>
        <input id="send_button" class="w3-btn w3-blue" type="button" value="Send">
      </form>
    </div>
    <div class="contact_footer">
        <p>Your contact information stays safe with me.</p>
        <p>Data you provide will not be shared or sold.</p>
    </div>
</div>