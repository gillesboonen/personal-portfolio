<?php
?>
<div id="support">
    <div class="page_content">
        <img id="return_icon" src="./img/return.png" alt="return">
        <div class="select_content_header">
            <h2><b>Technical <br /> Support</b></h2>
            <p>"What I love most about providing technical support is the satisfaction of problem-solving and the gratitude people show when resolving their issue(s)."</p>
        </div>
        <div class="select_content_proficiency">
            <h3>Proficiency</h3>
            <ul class="proficiency_ul">
                <li>
                    <div class="proficiency_text">Windows 10/8/7/XP</div>
                    <div class="stars_wrapper">
                        <img class="stars_icon" src="../img/stars_4.png" alt="PHP">
                        <p>Advanced</p>
                    </div>                        
                </li>
                <li>
                    <div class="proficiency_text">Linux (variety of distributions)</div>
                    <div class="stars_wrapper">
                        <img class="stars_icon" src="../img/stars_2.png" alt="PHP">
                        <p>Working knowledge</p>
                    </div>
                </li>
                <li>
                    <div class="proficiency_text">Active Directory</div>
                    <div class="stars_wrapper">
                        <img class="stars_icon" src="../img/stars_3.png" alt="PHP">
                        <p>Intermediate</p>
                    </div>
                </li>
                <li>
                    <div class="proficiency_text">Office 365 - MS Office Suite</div>
                    <div class="stars_wrapper">
                        <img class="stars_icon" src="../img/stars_3.png" alt="PHP">
                        <p>Intermediate</p>
                    </div>
                </li>
            </ul>
        </div>
        <div class="select_content_experience">
            <h3>Experience</h3>
            <ul class="experience_ul">
                <li><p><a target="_blank" href="https://first.eu">First IT</a>: 2 years as an <b>IT consultant</b> providing in-office IT support to employees from <a target="_blank" href="https://www.molenbergnatie.com/">Molenbergnatie</a> (Antwerp) and <a target="_blank" href="https://rtx.com">UTC</a> Brussels (now Raytheon Technologies)</p></li>
                <li><p><a target="_blank" href="https://www8.hp.com/">HP</a>, Ireland: 10 months as a <b>Technical Support Analyst</b> providing remote support to employees of business clients</p></li>
            </ul>
        </div>
        <div class="select_content_competences_support">
            <h3>Competences</h3>
            <ul class="competences_ul">
                <li><p>Natural flair for <b>problem-solving</b>, using <b>out of the box thinking</b> and <b>creativity</b></p></li>
                <li><p>Configuring <b>a multitude of different digital and electronic devices</b> and <b>resolving</b> their issues</p></li>
                <li><p><b>Communicating</b> details of technical issues and resolutions in a <b>non-technical</b> and <b>empathic</b> manner</p></li>
                <li><p><b>Helping</b> people no matter the issue, be it technical or different</p></li>
            </ul>
        </div>
    </div>  
</div>    
