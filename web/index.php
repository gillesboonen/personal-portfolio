<?php
if (session_status() == PHP_SESSION_NONE) {
  session_start();
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" type="text/css" href="css/index.css">
        <title>Gilles Boonen</title>
        <script type="text/javascript">
            if (screen.width <= 720) {
            window.location = "mobile/index.php";
        }
        </script>
    </head>
    <body>
        <a id="top"></a>
        <div id="navigation">   
          <a id="nav_home" href="#top">Gilles Boonen</a>
          <nav>
              <ul class="nav_links">
                  <li><a id="about_nav" class="nav_buttons" name="about" >about</a></li>
                  <li><a id="skills_nav" class="nav_buttons" name="skills">skills</a></li>
              </ul>
          </nav>
          <button id="contact_btn">contact</button>
        </div>
        <div id="root">
          <div id="intro">
          </div>    
          <div class="section_divider"></div>
          <div id="about">
          </div>    
          <div class="section_divider"></div>
          <div id="selection">
          </div>  
          <div id="select_content">
          </div>    
        </div>           
        <div id="contact" class="modal">
            <div class="modal-content">
              <div class="modal-header">
                <span class="close">&times;</span>
                  <h2>get in touch</h2>
              </div>
              <div class="modal-body">
                <form id="contact_form" action="POST">
                  <input class="w3-input contact-input" type="text" name="name" placeholder="Name">
                  <input class="w3-input contact-input" type="email" name="email" placeholder="E-mail">
                  <textarea class="w3-textarea contact-input" name="text_body" id="" cols="30" rows="8" placeholder="Text"></textarea>
                  <input id="send_button" class="w3-btn w3-blue" type="button" value="Send">
                </form>
              </div>
              <div class="modal-footer">
                <p>Your contact information stays safe with me.</p>
                <p>Data you provide will not be shared or sold.</p>
              </div>
            </div>
        </div>    
        <script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous"></script>
        <script src="js/index.js"></script>
      </body>
</html>