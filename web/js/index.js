
  function xhrGetIntro() {
    return $.ajax({
      type: 'POST',
      url: '../pages/intro.php',
      success: function(data) {
        $('#intro').html(data);
      }
    });
  }

  function xhrGetAbout() {
    return $.ajax({
      type: 'POST',
      url: '../pages/about.php',
      success: function(data) {
        $('#about').html(data);
      }
    });
  }

  function xhrGetSoftware() {
    return $.ajax({
      type: 'POST',
      url: '../pages/software.php',
      success: function(data) {
        $('#select_content').html(data);
      }
    });
  }

  function xhrGetSupport() {
    return $.ajax({
      type: 'POST',
      url: '../pages/support.php',
      success: function(data) {
        $('#select_content').html(data);
      }
    });
  }


  function xhrGetSelection() {
    return $.ajax({
      type: 'POST',
      url: '../pages/selection.php',
      success: function(data) {
        $('#selection').html(data);
      }
    });
  }

  function xhrGetSkills() {
    return $.ajax({
      type: 'POST',
      url: '../pages/skills.php',
      success: function(data) {
        $('#skills').html(data);
      }
    });
  }

  function xhrSubmitForm() {
    return $.ajax({
      type: 'POST',
      data: $('#contact_form').serialize(),
      url: '../contact/submit_form.php',
      success: function(data) {
        if (data == "Mail sent") {
          alert("Thank you for contacting me, the form was sent succesfully. \nI will get back to you as soon as possible.");
          setTimeout(() => {
            $("#contact").fadeOut();
            
          }, 1000);
        } else {
          alert("There was an issue sending the contact form, please try again later.");          
          setTimeout(() => {
            $("#contact").fadeOut();        
          }, 1000);
        }
      }
    });
  }

  function isScrolledIntoView(elem)
{
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}

$(document).ready(function() { 
  var activePage;

  xhrGetIntro().done(function(){});
  xhrGetAbout().done(function(){});
  xhrGetSelection().done(function(){});

  $("#nav_home").on("click", function(){
    $(".nav_buttons").removeClass("active");
    var pageName = $(this).attr('name');      
    $('html, body').animate({ scrollTop: 0 }, 500);
    $(this).addClass("active");   
    activePage = pageName;  
    window.location = '#'+pageName; 
  });

  $( ".nav_buttons" ).each(function() {
    $(this).on("click", function(){
        var pageName = $(this).attr('name');
        if (pageName == "about") {
          var scrollTop = $("body").scrollTop() + $("#"+pageName).offset().top;      
          $('html, body').animate({
              scrollTop: scrollTop
          }, 500);  
          activePage = pageName;  
          window.location = '#'+pageName;
        } else if (pageName == "skills") {
          $("#select_content").css("display", "none");
          $("#selection").css("display", "flex");
          pageName = "selection";
          var scrollTop = $("body").scrollTop() + $("#"+pageName).offset().top;      
          $('html, body').animate({
              scrollTop: scrollTop
          }, 500);  
          activePage = pageName;  
          window.location = '#'+pageName;

        } 
    });
  });


  $( "body" ).scroll(function() {
      if (isScrolledIntoView($("#intro"))) {
        console.log("hello");
        $("#nav_home").css({opacity: 1.0, visibility: "hidden"}).animate({opacity: 0.0}, 1000);
    } else {
      console.log("goodbye");
      $("#nav_home").css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1.0}, 2000);
    }
  })


  $(document.body).on('click',"#programming_select",function (event) { 
    $("#selection").css("display", "none");
    $("#select_content").css("display", "flex");
    xhrGetSoftware().done(function(){});
    // window.history.pushState("", "", './software.php');
  });

  $(document.body).on('click',"#support_select",function (event) { 
    $("#selection").css("display", "none");
    $("#select_content").css("display", "flex");
    xhrGetSupport().done(function(){});
    // window.history.pushState("", "", './support.php');
  });

  $(document.body).on('click',"#return_icon",function (event) { 
    $("#select_content").css("display", "none");
    $("#selection").css("display", "flex");
  });
  
  $(document.body).on('click',"#create_db_button",function (event) {
    event.preventDefault();
    console.log("clicked");
    $db_name = $("#create_db_name").val();
    xhrCreateDB($db_name).done(function() {
      xhrShowEmployeeTable($db_name).done(function() {});
    });
    $(".create_db_form").css("display", "none");
  });

  $(document.body).on('click',"#create_employee_button",function (event) {
    event.preventDefault();
    $db_name = $(this).attr("name");  
    xhrAddEmployee($db_name).done(function() {
      xhrShowEmployeeTable($db_name).done(function() {});});
  });

  $(document.body).on('click',".delete_employee",function (event) {
      event.preventDefault();
      $db_name = $(this).attr("name");  
      $id = $(this).attr("id");  
      console.log($id);
      xhrDeleteEmployee($db_name, $id).done(function() {
        xhrShowEmployeeTable($db_name).done(function() {});});
  });

  $(document.body).on('click',".edit_employee",function (event) {
    event.preventDefault();
    $(".td_save_edit_employee").css("display", "inline-block");
    $(".td_edit_employee").css("display", "none");
    $('input[name ="edit_first_name"]').prop("readonly", false);
    $('input[name ="edit_last_name"]').prop("readonly", false);
    $('input[name ="edit_employee_age"]').prop("readonly", false);
    $('input[name ="edit_employee_salary"]').prop("readonly", false);
    $('input[name ="edit_employee_role"]').prop("readonly", false);
    $('.edit_employee_input').removeClass("input_noneditable");
  });

  $(document.body).on('click',".save_edit_employee",function (event) {
    event.preventDefault();
    $db_name = $(this).attr("name");  
    $id = $(this).attr("id");  
    $(".td_save_edit_employee").css("display", "none");
    $(".td_edit_employee").css("display", "inline-block");
    xhrEditEmployee($db_name, $id).done(function() {
      xhrShowEmployeeTable($db_name).done(function() {});});
      $('.edit_employee_input').addClass("input_noneditable");
      $('input[name ="edit_first_name"]').prop("readonly", true);
      $('input[name ="edit_last_name"]').prop("readonly", true);
      $('input[name ="edit_employee_age"]').prop("readonly", true);
      $('input[name ="edit_employee_salary"]').prop("readonly", true);
      $('input[name ="edit_employee_role"]').prop("readonly", true);
  });

});

 // Javascript Modal functions

window.onload = function() {
  // Get the modal
var modal = document.getElementById("contact");
// Get the button that opens the modal
var btn = document.getElementById("contact_btn");
// Get the button that submits the form
var submit_btn = document.getElementById("send_button");
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];
// When the user clicks on the button, open the modal
btn.onclick = function() {
  modal.style.display = "block";
  $('#send_button').prop("disabled", false);
}

submit_btn.onclick = function() {
  $('#send_button').prop("disabled", true);
  xhrSubmitForm().done(function() {});
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}

}