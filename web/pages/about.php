<?php
// include $_SERVER['DOCUMENT_ROOT'].'/classes/aboutview.class.php';
?>
<div id="about_container">
    <div class="page_content_about">
        <div id="about_image" class="left_page">
            <img id="picture_gilles" src="../img/picture_gilles.JPG" alt="Gilles in Valencia">
            <div id="about_icons">

                <div class="about_resume">
                    <a target="_blank" href="https://www.linkedin.com/in/gilles-boonen-72738452/"><img src="../img/linkedin.png" alt="LinkedIn"></a>
                    <p>LinkedIn</p>
                </div>
                <div class="divider"></div>
                <div class="about_resume">
                    <a target="_blank" href="../pdf/Resume_Gilles_Boonen.pdf"><img src="../img/pdf.png" alt="Resume"></a>
                    <p>Resume</p>
                </div>
                <div class="divider"></div>
                <div class="about_resume">
                    <a target="_blank" href="https://gitlab.com/gillesboonen"><img src="../img/gitlab.png" alt="GitLab"></a>
                    <p>GitLab</p>
                </div>
            </div>
        </div>
        <div class="right_page">
            <div class="right_content">
                <p>Hi! <br /><br />
                    My name is Gilles, and I am a digital native. <br /><br />
                    
                    Ever since I was little, I've always had an interest in computers, consoles and electronic devices.<br />
                    From feeding my inner Picasso using MS Paint on a Windows 95 machine, trying to install ‘free’ foreign video games (on a Windows 98), 
                    learning to re-install the computer (thanks to those video games),
                    all the way to today;<br /> fascinated by creating, configuring or problem-solving whatever digital construct comes my way!
                    <br /><br />
                    In 2017 I embarked on a soul-searching adventure which led me to Madrid; the bustling capital where I enjoyed video game testing, and to Valencia; the beautiful coastal city with delightful beaches, nature and tranquility where I taught myself to program.
                    <br /><br />
                    After a wonderful 3 years abroad I am back in Belgium and ready to start working either in software development or IT support.
                    <br /><br />
                    Make your pick below to find out more about me!
                </p>
            </div>
        </div>
    </div>
</div>
