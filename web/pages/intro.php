<?php

?>
<div id="intro_container">
    <div class="page_content_intro">
        <div class="left_page_intro">
            <div class="page_header_intro">
                <p class="page_title_intro">Gilles Boonen</p>
                <p class="page_subtitle">Digital Native</p>
                <p class="page_subtitle">Looking for new opportunities</p>
            </div>
        </div>
        <div class="right_page">
        </div>
        <span id="photo_credit_intro">Photo by <a target="_blank" href="https://unsplash.com/@wizwow?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Donald Giannatti</a> on <a target="_blank" href="https://unsplash.com/s/photos/technology?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Unsplash</a></span>
    </div>
</div>