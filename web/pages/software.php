<?php
?>

<div id="software">
    <div class="page_content">
        <div class="left_page_skills">
            <img id="return_icon" src="../img/return.png" alt="return">
            <div class="select_content_header">
                <h1><b>Software Development</b></h2>
                <p>"Getting functionalities to work after spending hours on it gives a tremendous rush and immense satisfaction. <br /> The learning process is intensely active and rewards itself on a daily basis."</p>
            </div>
            <div class="select_content_proficiency">
                <h2>Proficiency</h2>
                <ul class="proficiency_ul">
                    <li>
                        <div class="proficiency_text">PHP</div>
                        <div class="stars_wrapper">
                            <img class="stars_icon" src="../img/stars_3.png" alt="PHP">
                            <p>Intermediate</p>
                        </div>                        
                    </li>
                    <li>
                        <div class="proficiency_text">HTML & CSS</div>
                        <div class="stars_wrapper">
                            <img class="stars_icon" src="../img/stars_3.png" alt="PHP">
                            <p>Intermediate</p>
                        </div>
                    </li>
                    <li>
                        <div class="proficiency_text">JavaScript & jQuery</div>
                        <div class="stars_wrapper">
                            <img class="stars_icon" src="../img/stars_3.png" alt="PHP">
                            <p>Intermediate</p>
                        </div>
                    </li>
                    <li>
                        <div class="proficiency_text">MySQL</div>
                        <div class="stars_wrapper">
                            <img class="stars_icon" src="../img/stars_2.png" alt="PHP">
                            <p>Working knowledge</p>
                        </div>
                    </li>
                    <li>
                        <div class="proficiency_text">Python & Flask</div>
                        <div class="stars_wrapper">
                            <img class="stars_icon" src="../img/stars_2.png" alt="PHP">
                            <p>Working knowledge</p>
                        </div>
                    </li>
                </ul>
            </div>

        </div>
        <div class="right_page_skills">
        <div class="select_content_experience">
                <h2>Experience</h2>
                <ul class="experience_ul">
                    <li><p>2 years teaching myself to program and working on an <b>invoicing platform</b>, work name: <a target="_blank" href="https://app.lemonchocolate.biz">LemonChocolate</a></p>
                    <p>The platform has been in the making <b>since November 2018</b>, with the help of co-founder, good friend and great mentor Dennis Brul (<a target="_blank" href="https://lemon3w.biz/">Lemon3W</a>)</p>
                    <p>Dennis handles the front-end, while I take care of the <b>back-end</b>, using <b>PHP</b> and <b>MySQL</b>.</p></li>
                    <li><p>Coding along <b>various tutorials</b>, experimenting with <b>different languages and frameworks</b> to get an introductory working knowledge (<a target="_blank" href="https://gitlab.com/gillesboonen/project-health">Python & Flask</a>, <a target="_blank" href="https://gitlab.com/gillesboonen/react-native-tutorial---2---guess-app">React Native</a>) </p></li>
                </ul>
            </div>
            <div class="select_content_competences_software">
                <h2>Competences</h2>
                <ul class="competences_ul">
                    <li><p><b>Swiftly adding</b> to existing <b>knowledge</b> about languages</p></li>
                    <li><p>Picking up <b>new technologies and frameworks</b> and being able to <b>create</b> with them in a matter of hours</p></li>
                    <li><p><b>Resolution</b> to tackle problems faced, and the <b>energy</b> to not stop until done</p></li>
                    <li><p><b>Ability to let go</b> of a certain strategy if it does not succeed, and <b>come up with different ways</b> to resolve the same thing</p></li>
                    <li><p>Enormous <b>drive to learn</b> from experienced people</p></li>
                </ul>
            </div>
            
        </div>
    </div>  
</div>    
