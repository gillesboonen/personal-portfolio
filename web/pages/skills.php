<?php
 // include $_SERVER['DOCUMENT_ROOT'].'/classes/skillsview.class.php';
?>
<div id="skills_container">
    <div class="page_content">
        <div class="left_page">
            <div class="page_header">
                <p class="page_title">skills</p>
            </div>

        </div>
        <div class="right_page">
            <div class="right_content">
                <div class="skill_frame">
                    <p class="skill_title">Back-end</p>
                    <div class="skill_unit">PHP <b>+++</b></div>
                    <div class="skill_unit">Python <b>+</b></div>
                    <div class="skill_unit">Ruby on Rails <b>+</b></div>
                    <div class="skill_unit">.NET <b>+</b></div>
                    <div class="skill_unit">C# <b>+</b></div>
                </div>
                <div class="skill_frame">
                    <p class="skill_title">Front-end</p>
                    <div class="skill_unit">HTML <b>++</b></div>
                    <div class="skill_unit">CSS <b>++</b></div>
                    <div class="skill_unit">jQuery <b>++</b></div>
                </div>
                    <div class="skill_frame">
                    <p class="skill_title">DB</p>
                    <div class="skill_unit">SQL <b>++</b></div>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
